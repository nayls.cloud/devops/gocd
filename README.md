# GOCD example project

## Plugins

+ Analytics plugins:
  - [gocd-analytics-plugin](https://github.com/gocd/gocd-analytics-plugin/releases/tag/v3.0.0-5)

+ Artifacts plugins:
  - [docker-registry-artifact-plugin](https://github.com/gocd/docker-registry-artifact-plugin/releases/tag/v1.2.0-127)

+ Authorization plugins:
  - [gocd-groovy-dsl-config-plugin](https://github.com/gocd-contrib/gocd-groovy-dsl-config-plugin/releases/tag/v2.0.0-241)
  - [gocd-yaml-config-plugin](https://github.com/tomzo/gocd-yaml-config-plugin/releases/tag/0.13.0)

+ Notification plugins:
  - [email-notifier](https://github.com/gocd-contrib/email-notifier/releases/tag/v1.0.0-74-exp)
  - [gocd-http-notifications-plugin](https://github.com/matic-insurance/gocd-http-notifications-plugin/releases/tag/0.1.2)
  - [gocd-build-status-notifier](https://github.com/gocd-contrib/gocd-build-status-notifier/releases/tag/1.7.0-100-exp)

+ Package repository plugins
  - [go-npm-poller](https://github.com/varchev/go-npm-poller/releases/tag/0.3.1)
  - [deb-repo-poller](https://github.com/gocd-contrib/deb-repo-poller/releases/tag/1.4)

+ SCM plugins
  - [gocd-build-github-pull-requests](https://github.com/ashwanthkumar/gocd-build-github-pull-requests/releases/tag/v1.3.5)
  - [gocd-build-github-pull-requests](https://github.com/ashwanthkumar/gocd-build-github-pull-requests/releases/tag/v1.3.5)

+ Secrets plugins
  - [gocd-file-based-secrets-plugin](https://github.com/gocd/gocd-file-based-secrets-plugin/releases/tag/v1.0.0-33)
  - [gocd-vault-secret-plugin](https://github.com/gocd/gocd-vault-secret-plugin/releases/tag/v1.1.0-31)

+ Task plugins
  - [gocd-docker](https://github.com/manojlds/gocd-docker/releases/tag/0.1.27)


## Screenshots

[![](docs/Dashboard.png)](docs/Dashboard.png)
[![](docs/VSM.png)](docs/VSM.png)
[![](docs/PipelineActivity.png)](docs/PipelineActivity.png)
[![](docs/Build.png)](docs/Build.png)
[![](docs/BuildLogs.png)](docs/BuildLogs.png)
[![](docs/PipelineStatusPage.png)](docs/PipelineStatusPage.png)
